//
//  UIViewController+Extensions.swift
//  IOASYS_Login
//
//  Created by Gabriel Henrique on 02/06/19.
//  Copyright © 2019 Gabriel Bernardo. All rights reserved.
//

import UIKit

extension UIViewController {
    @IBAction func unwind(_ segue: UIStoryboardSegue ){}
}
